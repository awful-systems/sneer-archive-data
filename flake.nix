{
  description = "r/SneerClub's contents preserved as JSON files";

  inputs = { flake-utils.url = "github:numtide/flake-utils"; };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages."${system}";
        process-markdown = pkgs.writeText "process-markdown.mlr" ''
          subr process_replies(r) {
               for (i, v in $r) {
                   b = r[i]["body"];
                   md = system("${pkgs.pandoc}/bin/pandoc -f markdown <<__EOF__\n".$r[i]["body"]."\n__EOF__");
                   utc = $r[i]["created_utc"];
                   if (!is_error(md)) {
                      $r[i]["body"] = md;
                   }
                   $r[i]["created_date"] = system("date -d @".utc." -u +'%B %d, %Y %I:%M %p'");

                   call process_replies($r[i].replies);
               }
          }
          $selftext = system("${pkgs.pandoc}/bin/pandoc -f markdown <<__EOF__\n".$selftext."\n__EOF__");
          $created_date = system("date -d @".$created_utc." -u +'%B %d, %Y %I:%M %p'");
          for (i, v in $comments) {
              b = $comments[i].body;
              md = system("${pkgs.pandoc}/bin/pandoc -f markdown <<__EOF__\n".b."\n__EOF__");
              utc = $comments[i].created_utc;
              if (!is_error(md)) {
                 $comments[i].body = md;
              }
              $comments[i].created_date = system("date -d @".utc." -u +'%B %d, %Y %I:%M %p'");

              call process_replies($comments[i]["replies"]);
          }
        '';
      in {
        packages.json-threads = pkgs.runCommand "process-json" { } ''
          shopt -s globstar
          mkdir -p $out
          tar -I ${pkgs.zstd}/bin/zstd -xf ${./bdfr.tar.zst}
          ${pkgs.miller}/bin/mlr --json \
                                 put -f ${process-markdown} then \
                                 sort -nr created_utc \
                                 ./bdfr/**/*.json > $out/threads-newest.json
          ${pkgs.miller}/bin/mlr --json \
                                 cut -x -f comments,selftext then \
                                 sort -nr score \
                                 $out/threads-newest.json > $out/submissions-bestest.json
          ${pkgs.miller}/bin/mlr --json \
                                 cut -x -f comments,selftext then \
                                 sort -nr num_comments \
                                 $out/threads-newest.json > $out/submissions-longest.json
        '';

        packages.default = self.packages."${system}".json-threads;

      });
}
